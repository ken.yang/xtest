# -*- coding: utf-8 -*-

from app.core.config import Config
from app.core.log import Log


class BaseApp:
    """
    顶层基础类，可继承方便调用
    """

    def __init__(self):
        # 日志
        self.log = Log().init()

        # 配置文件
        self.config = Config().init()
