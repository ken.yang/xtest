# -*- coding: utf-8 -*-

from app.lib.email_runner.email_text import EmailText
from app.lib.email_runner.email_html import EmailHtml
from app.lib.email_runner.email_file import EmailFile


class EmailRunner():
    """
    邮件发送类
    """

    @staticmethod
    def make(msg_type, subject="", msg_from="", msg_to=""):
        """
        简单工场
        :param msg_type: string 消息类型
        :param subject: string 邮件标题
        :param msg_from: string 发送者名字
        :param msg_to: string 接受者名字
        :return:
        """
        if msg_type == "text":
            return EmailText(subject, msg_from, msg_to)
        elif msg_type == "html":
            return EmailHtml(subject, msg_from, msg_to)
        elif msg_type == "file":
            return EmailFile(subject, msg_from, msg_to)

    def make_text(self, subject="", msg_from="", msg_to=""):
        return self.make("text", subject, msg_from, msg_to)

    def make_html(self, subject="", msg_from="", msg_to=""):
        return self.make("html", subject, msg_from, msg_to)

    def make_file(self, subject="", msg_from="", msg_to=""):
        return self.make("file", subject, msg_from, msg_to)
