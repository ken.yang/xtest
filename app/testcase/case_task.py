# -*- coding: utf-8 -*-

import json
import unittest

import jsonpath as jsonpath
import requests as requests

from app.base_case import BaseCase
from app.testdata.base_data import BASE_HEADERS
from app.testdata.login_data import LOGIN_SUCCESS_DATA
from app.testdata.task_data import TASK_LIST_DATA, TASK_ADD_DATA


class CaseTask(BaseCase, unittest.TestCase):
    """
    任务测试

    顺序执行：test执行的时候是按照名字来的，所以多个test要顺序执行，最好添加a、b、c、d这样的字母到方法名中

    上下文管理：BaseCase中添加了类变量context，使用的时候只需要使用self.context即可
    """

    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        BaseCase.__init__(self)

    def test_a_login(self):
        """登录"""

        # 1.发起请求
        response = requests.post(LOGIN_SUCCESS_DATA["url"], headers=LOGIN_SUCCESS_DATA["headers"],
                                 data=LOGIN_SUCCESS_DATA["data"])
        print(response.text)

        # 2.处理数据
        status_code = jsonpath.jsonpath(json.loads(response.text), "$..status_code")[0]
        message = jsonpath.jsonpath(json.loads(response.text), "$..message")[0]

        # 3.断言
        self.assertEqual(status_code, 200, str(message))

        # 4.上下文处理
        self.context["token"] = jsonpath.jsonpath(json.loads(response.text), "$..token")[0]

    def test_b_paper_task_list(self):
        """获取任务列表"""

        # 1.数据处理
        BASE_HEADERS["token"] = self.context["token"]

        # 2.发起请求
        response = requests.get(TASK_LIST_DATA["url"], headers=BASE_HEADERS, data=TASK_LIST_DATA["data"])
        print(response.text)

        # 3.处理数据
        status_code = jsonpath.jsonpath(json.loads(response.text), "$..status_code")[0]
        message = jsonpath.jsonpath(json.loads(response.text), "$..message")[0]
        data_list = jsonpath.jsonpath(json.loads(response.text), "$..data.data")

        # 4.断言
        self.assertEqual(status_code, 200, message)
        self.assertIsNotNone(data_list, "列表为空")

        # 5.上下文处理
        self.context["task_id"] = jsonpath.jsonpath(json.loads(response.text), "$..id")[0]

    def test_c_paper_task_operate(self):
        """获取任务列表"""

        # 1.数据处理
        BASE_HEADERS["token"] = self.context["token"]
        TASK_ADD_DATA["data"]["task_id"] = self.context["task_id"]

        # 2.发起请求
        response = requests.post(TASK_ADD_DATA["url"], headers=BASE_HEADERS, data=TASK_ADD_DATA["data"])
        print(response.text)

        # 3.处理数据
        status_code = jsonpath.jsonpath(json.loads(response.text), "$..status_code")[0]
        message = jsonpath.jsonpath(json.loads(response.text), "$..message")[0]

        # 4. 断言
        self.assertEqual(status_code, 200, message)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(CaseTask))
    unittest.TextTestRunner(verbosity=2).run(suite)
