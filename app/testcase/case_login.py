# -*- coding: utf-8 -*-

import json
import unittest

import jsonpath as jsonpath
import requests as requests

from app.base_case import BaseCase
from app.testdata.login_data import LOGIN_SUCCESS_DATA, LOGIN_ERROR_DATA


class CaseLogin(BaseCase, unittest.TestCase):
    """
    登录接口测试

    顺序执行：test执行的时候是按照名字来的，所以多个test要顺序执行，最好添加a、b、c、d这样的字母到方法名中

    上下文管理：BaseCase中添加了类变量context，使用的时候只需要使用self.context即可
    """

    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        BaseCase.__init__(self)

    def setUp(self) -> None:
        pass

    def test_login_success(self):
        """登录成功测试"""

        # 1.发起请求
        response = requests.post(LOGIN_SUCCESS_DATA["url"], headers=LOGIN_SUCCESS_DATA["headers"],
                                 data=LOGIN_SUCCESS_DATA["data"])
        print(response.text)

        # 2.处理数据
        status_code = jsonpath.jsonpath(json.loads(response.text), "$..status_code")[0]

        # 3. 断言
        self.assertEqual(status_code, 200, "登录失败")

        # 4. 生成值
        token = jsonpath.jsonpath(json.loads(response.text), "$..token")[0]
        self.log.info("登录token：" + token)

    def test_login_error(self):
        """登录失败测试"""

        # 1.发起请求
        response = requests.post(LOGIN_ERROR_DATA["url"], headers=LOGIN_ERROR_DATA["headers"],
                                 data=LOGIN_ERROR_DATA["data"])
        print(response.text)

        # 2.处理数据
        status_code = jsonpath.jsonpath(json.loads(response.text), "$..status_code")[0]

        # 3. 断言
        self.assertEqual(status_code, 401, "出现其他错误")

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(CaseLogin))
    unittest.TextTestRunner(verbosity=2).run(suite)
