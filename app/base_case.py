# -*- coding: utf-8 -*-

from app.base_app import BaseApp


class BaseCase(BaseApp):
    """
    case基类，所有case都需要继承
    """

    # 上下文管理
    context = {}

    def __init__(self):
        super(BaseCase, self).__init__()
