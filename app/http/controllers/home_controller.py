# -*- coding: utf-8 -*-

from flask import render_template

from app.http.controllers.base_controller import BaseController


class HomeController(BaseController):

    def __init__(self):
        super().__init__()

    def home(self):
        return render_template("home.html")
