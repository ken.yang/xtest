# -*- coding: utf-8 -*-

from flask import render_template, request

from app.http.controllers.base_controller import BaseController
from app.http.services.report_service import ReportService


class ReportController(BaseController):

    def __init__(self):
        super().__init__()
        self.report_service = ReportService()

    def info(self):
        """测试报告结果"""

        report_name = request.args.get("report_name")
        report_html = self.report_service.info(report_name)

        return report_html

    def list(self):
        """测试报告列表"""

        report_list = self.report_service.list()

        return render_template("report/list.html", report_list=report_list, delete_message="")

    def delete(self):
        """删除测试报告"""

        report_name = request.args.get("report_name")
        delete_message = self.report_service.delete(report_name)

        report_list = self.report_service.list()

        return render_template("report/list.html", report_list=report_list, delete_message=delete_message)
