# -*- coding: utf-8 -*-

from app.base_app import BaseApp


class BaseController(BaseApp):
    """
    case基类，所有case都需要继承
    """

    def __init__(self):
        super(BaseController, self).__init__()
