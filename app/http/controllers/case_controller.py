# -*- coding: utf-8 -*-
import os

from flask import render_template, request

from app.helper.helper import report_path
from app.http.controllers.base_controller import BaseController
from app.http.services.case_service import CaseService
from app.lib.excel.excel import Excel


class CaseController(BaseController):

    def __init__(self):
        super().__init__()

    def rule(self):
        """查看规则"""

        return render_template("case/rule.html")

    def report_desc(self):
        """测试报告结果"""

        report_name = request.args.get("report_name")
        report_filepath = os.path.join(report_path(), report_name)

        with open(report_filepath, "r+") as f:
            report_html = f.read()

        return report_html

    def run(self):
        """执行测试用例"""

        # 获取上传文件内容
        excel_file = request.files.get('excel_file')
        f = excel_file.read()

        # 初始化excel对象
        excel_data = Excel(file_contents=f)

        case_service = CaseService(excel_data.get_excel_data(1, 2))
        result = case_service.run()

        return render_template("case/run.html", result=result)
