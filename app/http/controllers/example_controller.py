# -*- coding: utf-8 -*-

from flask import request, abort

from app.http.controllers.base_controller import BaseController
from app.http.common.output import Output

TOKEN = "123456789"


class ExampleController(BaseController):

    def __init__(self):
        super().__init__()

    def home(self):
        return 'Welcome To Sample!'

    def login(self):
        """登录"""

        username = request.form['username']
        password = request.form['password']

        if username == "admin" and password == "123456":
            data = {"username": username, "token": TOKEN}
            return Output().success(data)
        else:
            abort(401)

    def task_list(self):
        """任务列表"""

        token = request.headers.get('token', None)
        if token != TOKEN:
            abort(401)

        data = [
            {
                "id": 1,
                "name": "task one",
                "time": "2020-04-01"
            },
            {
                "id": 2,
                "name": "task two",
                "time": "2020-04-02"
            },
            {
                "id": 3,
                "name": "task three",
                "time": "2020-04-03"
            },
        ]
        return Output().success(data)

    def task_add(self):
        """添加任务"""

        token = request.headers.get('token', None)
        if token != TOKEN:
            abort(401)

        task_id = int(request.form['task_id'])
        if task_id not in (1, 2, 3):
            return Output().message("task_id is error").error()
        return Output().success()
