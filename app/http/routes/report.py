# -*- coding: utf-8 -*-

from flask import Blueprint

from app.http.controllers.report_controller import ReportController

report_page = Blueprint('report_page', __name__)


@report_page.route('/info', methods=['GET'])
def report_desc():
    return ReportController().info()


@report_page.route('/list', methods=['GET'])
def report_list():
    return ReportController().list()


@report_page.route('/delete', methods=['GET'])
def report_delete():
    return ReportController().delete()
