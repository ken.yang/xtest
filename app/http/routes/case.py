# -*- coding: utf-8 -*-

from flask import Blueprint

from app.http.controllers.case_controller import CaseController

case_page = Blueprint('case_page', __name__)


@case_page.route('/rule', methods=['GET'])
def rule():
    return CaseController().rule()


@case_page.route('/run', methods=['POST'])
def run():
    return CaseController().run()
