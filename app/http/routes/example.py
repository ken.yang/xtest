# -*- coding: utf-8 -*-

from flask import Blueprint

from app.http.controllers.example_controller import ExampleController

example = Blueprint('example', __name__)


@example.route('/login/', methods=['POST'])
def login():
    return ExampleController().login()


@example.route('/task/list', methods=["GET"])
def task_list():
    return ExampleController().task_list()


@example.route('/task/add', methods=['POST'])
def task_add():
    return ExampleController().task_add()
