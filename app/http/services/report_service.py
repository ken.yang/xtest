# -*- coding: utf-8 -*-
import glob
import os

from app.helper.helper import report_path
from app.http.services.base_service import BaseService


class ReportService(BaseService):
    def __init__(self):
        super().__init__()

    def list(self):
        """测试报告列表"""

        report_list = []
        # 循环遍历config文件夹加载所有配置文件
        for report_file in glob.glob(os.path.join(report_path(), "*.html")):
            report_list.append(os.path.basename(report_file))

        return report_list

    def info(self, report_name):
        """测试报告详情"""

        report_filepath = os.path.join(report_path(), report_name)

        with open(report_filepath, "r+", encoding="utf-8") as f:
            report_html = f.read()

        return report_html

    def delete(self, report_name):
        """测试报告详情"""

        report_filepath = os.path.join(report_path(), report_name)

        if os.path.exists(report_filepath):
            os.remove(report_filepath)
            return "删除成功：" + report_name
        else:
            return "删除失败，文件已不存在"
