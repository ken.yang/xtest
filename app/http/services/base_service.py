# -*- coding: utf-8 -*-

from app.base_app import BaseApp


class BaseService(BaseApp):
    """
    case基类，所有case都需要继承
    """

    def __init__(self):
        super(BaseService, self).__init__()
