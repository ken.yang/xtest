# -*- coding: utf-8 -*-

import unittest

from ddt import ddt, data

from app.auto_case import AutoCase
from app.helper import app_global


@ddt
class CaseAutoTestService(AutoCase):
    """
    任务测试-通过excel获取数据进行接口测试--全封装自动化

    约定
    一个excel为：excel_data
    excel中的一个sheet为：sheet_data
    sheet中的一个case为：case_data
    """

    excel_data = app_global.auto_excel_data

    def __init__(self, methodName="runTest"):
        super().__init__(methodName)

    @data(*excel_data)
    def test_excel_all_sheet(self, sheet_data):
        """
        对excel所有sheet做测试
        """

        for case_data in sheet_data:
            self.run_case_test(case_data)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(CaseAutoTestService))
    unittest.TextTestRunner(verbosity=2).run(suite)
