# -*- coding: utf-8 -*-

class Output:
    """
    输出类
    """

    def __init__(self):
        self._data = None
        self._code = None
        self._message = None

    def data(self, data):
        self._data = data
        return self

    def message(self, message):
        self._message = message
        return self

    def code(self, code):
        self._code = code
        return self

    def info(self):
        out = {
            "status_code": self._code,
            "message": self._message,
            "data": self._data
        }
        return out

    def success(self, data=None):
        self._code = self._code if self._code else 200
        self._message = self._message if self._message else "success"
        self._data = self._data if self._data else data
        return self.info()

    def error(self, message="error", code=400):
        self._code = self._code if self._code else code
        self._message = self._message if self._message else message
        return self.info()
