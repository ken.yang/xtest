# -*- coding: utf-8 -*-

import os
from os.path import dirname

from app.helper.const import REPORT_FOLDER, TEST_CASE_FOLDER, DATA_FOLDER, TEMPLATE_FOLDER, STATIC_FOLDER


def root_path():
    """获取项目根目录"""

    return dirname(dirname(dirname(os.path.realpath(__file__))))


def report_path():
    """测试报告路径"""

    path = os.path.join(root_path(), REPORT_FOLDER)
    check_path(path)

    return path


def testcase_path():
    """测试用例路径"""

    path = os.path.join(root_path(), TEST_CASE_FOLDER)
    check_path(path)

    return path


def data_path():
    """excel文件路径"""

    path = os.path.join(root_path(), DATA_FOLDER)
    check_path(path)

    return path


def check_path(path):
    """检查路径，不存在则创建"""

    if not os.path.exists(path):
        os.makedirs(path)


def template_folder():
    """获取模板文件夹路径"""

    path = os.path.join(root_path(), TEMPLATE_FOLDER)

    return path


def static_folder():
    """获取静态文件文件夹路径"""

    path = os.path.join(root_path(), STATIC_FOLDER)

    return path
