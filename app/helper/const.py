# -*- coding: utf-8 -*-

# 测试报告存放文件夹
REPORT_FOLDER = "storage/report"

# 测试用例存放文件夹
TEST_CASE_FOLDER = "app/testcase"

# excel data存放文件夹
DATA_FOLDER = "storage/data"

# 视图模板文件夹
TEMPLATE_FOLDER = "app/http/views"

# 静态文件文件夹
STATIC_FOLDER = "public/assets"
