# -*- coding: utf-8 -*-

def Singleton(cls):
    """单列"""

    _instance = {}

    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton
