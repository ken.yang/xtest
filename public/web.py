#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

# python命令执行处理

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

from flask import Flask

from app.http.common.output import Output
from app.http.routes.example import example
from app.http.routes.case import case_page
from app.http.routes.report import report_page
from app.http.routes.home import home_page
from app.helper.helper import template_folder, static_folder
from app.core.config import Config

app = Flask(__name__, template_folder=template_folder(), static_folder=static_folder())

#
# 每添加一个路由文件，需要在这里进行注册
#

# 首页
app.register_blueprint(home_page)

# 测试用例
app.register_blueprint(case_page, url_prefix='/case')

# 测试用例
app.register_blueprint(report_page, url_prefix='/report')

# 演示
app.register_blueprint(example, url_prefix='/example')


@app.errorhandler(401)
def auth_fail(error):
    return Output().code(401).message("auth fail!").error()


@app.errorhandler(404)
def not_found(error):
    return Output().code(404).message("page not found").error()


if __name__ == '__main__':
    """
    http://127.0.0.1:5000/
    """
    c = Config().init()
    host = c.get("web", "host")
    port = c.get("web", "port")

    app.run(host=host, port=port)
