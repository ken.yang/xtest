# -*- coding: utf-8 -*-

import unittest
from app.base_app import BaseApp


class TestBaseApp(unittest.TestCase):
    """
    基础类测试
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.app = AppSon()

    def test_super_app_config(self):
        """测试config"""

        value = self.app.get_config()
        self.assertIsNotNone(value, "获取值失败")

    def test_super_app_log(self):
        """测试log"""

        value = self.app.log_print()
        self.assertIsNone(value, "返回None")


class AppName:
    """测试基类"""

    def __init__(self, name):
        # 多继承情况下，带参数的基类需要使用super
        super(AppName, self).__init__()
        self.name = name


class AppSon(AppName, BaseApp):
    """测试子类"""

    def __init__(self, name="name"):
        super(AppSon, self).__init__(name)

    def get_config(self):
        return self.config.get("mysql", "host")

    def log_print(self):
        return self.log.debug("i am default info")


if __name__ == "__main__":
    # verbosity=*：默认是1；设为0，则不输出每一个用例的执行结果；2-输出详细的执行结果
    unittest.main(verbosity=2)
