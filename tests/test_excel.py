# -*- coding: utf-8 -*-

import unittest
from app.lib.excel.excel import Excel


class TestExcel(unittest.TestCase):
    """
    excel测试
    """

    def test_get_sheet_data(self):
        """测试获取sheet数据"""

        e = Excel("data_中文演示.xls")
        sheet_data = e.get_sheet_data("测试1", 1, 2)

        self.assertIsNotNone(sheet_data)


if __name__ == "__main__":
    # verbosity=*：默认是1；设为0，则不输出每一个用例的执行结果；2-输出详细的执行结果
    unittest.main(verbosity=2)
