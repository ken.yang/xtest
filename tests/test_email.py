# -*- coding: utf-8 -*-
import os.path

from app.helper.helper import data_path
from app.lib.email_runner.email_runner import EmailRunner

import unittest
from app.core.config import Config


class TestEmail(unittest.TestCase):
    """
    邮件测试
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.c = Config().init()
        cls.email_runner = EmailRunner()

    def test_send_text_mail(self):
        """测试纯文本邮件"""

        r = self.email_runner.make("text", "纯文本邮件", "text", "测试text")

        r.set_message("纯文本邮件")
        result = r.sendmail()

        self.assertTrue(result)

    def test_send_html_mail(self):
        """测试富文本本邮件"""

        r = self.email_runner.make_html("富文本邮件", "html", "测试html")

        r.set_message("<p>富文本邮件</p>我是换行内容")
        result = r.sendmail()

        self.assertTrue(result)

    def test_send_file_mail(self):
        """测试带附件的富文本邮件"""

        r = self.email_runner.make_file("富文本--附件邮件", "file", "测试file")

        r.set_message("<p>富文本--附件邮件</p>我是换行内容", "html")
        r.attach_file(os.path.join(data_path(), "data_example.xls"), "data_ecample_for_mail.xls")

        result = r.sendmail()
        self.assertTrue(result)


if __name__ == "__main__":
    # verbosity=*：默认是1；设为0，则不输出每一个用例的执行结果；2-输出详细的执行结果
    unittest.main(verbosity=2)
