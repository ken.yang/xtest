FROM python:3.9

LABEL author="ken"
LABEL email="695093513@qq.com"
LABEL desc="dockerfile for xtest"

COPY . /www

WORKDIR /www

RUN pip install -r requirements.txt

WORKDIR /www/public

EXPOSE 5000

ENTRYPOINT ["python","web.py"]